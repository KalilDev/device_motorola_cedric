#
#    Copyright (C) 2017  Pedro Kalil
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
 
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk) 
$(call inherit-product, device/motorola/cedric/cedric.mk)

# Inherit some common XOS stuff.
$(call inherit-product, vendor/xos/config/common.mk)

PRODUCT_NAME := XOS_cedric
PRODUCT_RELEASE_NAME := Moto G (5)
PRODUCT_DEVICE := cedric
PRODUCT_BRAND := Motorola
PRODUCT_MANUFACTURER := Motorola
PRODUCT_MODEL := Moto G (5)
