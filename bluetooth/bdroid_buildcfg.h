/*
 *   Copyright (C) 2017  The Android Open Source Project
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 */

#ifndef _BDROID_BUILDCFG_H
#define _BDROID_BUILDCFG_H

#include <cutils/properties.h>
#include <string.h>

#define BTM_DEF_LOCAL_NAME "Moto G (5)"
#define BTA_DISABLE_DELAY 1000 /* in milliseconds */
#define BLUETOOTH_QTI_SW TRUE
#define MAX_L2CAP_CHANNELS    16
#define BLE_VND_INCLUDED   TRUE
#endif
